from django.shortcuts import render, get_object_or_404, redirect
from .models import HepsiInput
from .forms import ProductForm
from .forms import CustomerForm
from django.views import generic
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


# Create your views here.
class Startpage(generic.ListView):
    template_name = 'startpage.html'

    def get_queryset(self):
        return HepsiInput.objects.all()
    # def get_queryset(self):
    #     return Customer.objects.order_by('name'[:5])


class BiyografilerView(generic.ListView):
    template_name = 'biyografiler.html'

    def get_queryset(self):
        return HepsiInput.objects.all()


class ResimlerView(generic.ListView):
    template_name = 'about.html'

    def get_queryset(self):
        return HepsiInput.objects.all()
