from django.db import models

# Create your models here.


class HepsiInput (models.Model):
    title = models.CharField(max_length=100)
    price = models.CharField(max_length=100)

    # funktion damit man die detail ansicht vom jeweiligen produkt bekommt,
    # wenn man den link in der tabelle anclickt
    def get_absolute_url(self):
        return f"/product/{self.id}"

    # def __str__(self):
    #     return '{}'.format(self.title)


class HepsiInput2 (models.Model):
    name = models.CharField(max_length=100)
    industry = models.CharField(max_length=100)

    # funktion damit man die detail ansicht vom jeweiligen produkt bekommt,
    # wenn man den link in der tabelle anclickt
    def get_absolute_url(self):
        return f"/customer/{self.id}"

    # # on submit click on the product entry page, it redirects to the url below.
    # def get_absolute_url(self):
    #     return reverse('modelforms:index')
